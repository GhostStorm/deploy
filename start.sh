#/bin/bash

docker-compose up -d vnode
docker-compose up -d rabbitmq
docker-compose up -d mongo_db
docker-compose up -d rest_gateway
